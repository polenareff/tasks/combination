import cv2, imageio, numpy as np
from math import asin, pi, sqrt, e, sin, cos, tan

MIN_ALT = 10
MIN_CANVAS_M = 4500
MAX_ROLL = 4
MAX_PITCH = 4
TILT_CORRECTION = .0 # Ignore roll & pitch for now

CANVAS_H = 5000
CANVAS_W = 5000
CANVAS_H_MARGIN = 1000
CANVAS_W_MARGIN = 1000

GIF_BACKGROUND_COLOR_GRAYSCALE = 200

def rotate(image, deg):
    h, w = image.shape[:2]
    c_y, c_x = h//2, w//2
    rot_mat = cv2.getRotationMatrix2D((c_x, c_y), deg, 1.0)
    cos = abs(rot_mat[0, 0])
    sin = abs(rot_mat[1, 0])
    h, w = int(h * cos + w * sin), int(h * sin + w * cos)
    rot_mat[0, 2] += (w / 2) - c_x
    rot_mat[1, 2] += (h / 2) - c_y
    return cv2.warpAffine(image, rot_mat, (w, h))

def blend_by_dst(pix1, dst1, pix2, dst2, fast_mode=False):
    if fast_mode:
        return pix1 if dst1 < dst2 else pix2
    if dst1 + dst2 == 0:
        return pix1
    k = dst2 / (dst1 + dst2)
    k = 1 / (1 + e ** (-10*(k - .5)))
    return [int(a * k) + int(b * (1 - k)) for a, b in zip(pix1, pix2)]

def combine(metas, images, canvas_m, foc_len, gif, fast_mode, nocrop):
    cdst = np.zeros((CANVAS_H, CANVAS_W), np.uint8)
    canvas = np.zeros((CANVAS_H, CANVAS_W, 4), np.uint8)
    canvas[:,:,:3].fill(GIF_BACKGROUND_COLOR_GRAYSCALE)
    h, w = 0, 0
    min_i = CANVAS_H
    max_i = 0
    min_j = CANVAS_W
    max_j = 0
    for meta in metas:
        image = images[meta['name']]
        h0, w0 = image.shape[:2]

        # TODO: Update constants?
        Wm = 12.8
        Hm = 9.6
        W = meta['alt'] * Wm / foc_len
        Wpx = W * (CANVAS_W - 2 * CANVAS_W_MARGIN) / canvas_m
        scale_factor = Wpx / w0
        image = cv2.resize(image, (0, 0), fx=scale_factor, fy=scale_factor)

        w0 *= scale_factor
        h0 *= scale_factor
        dst_max = int(.7 * sqrt(h0 ** 2 + w0 ** 2) / 2)

        image = rotate(image, -meta['yaw'])
        h, w = image.shape[:2]

        pitch_v = [cos(meta['yaw'] - 90), sin(meta['yaw'] - 90)]
        roll_v = [pitch_v[1], -pitch_v[0]]

        pitch_v[0] *= TILT_CORRECTION * meta['alt'] * (CANVAS_W - 2 * CANVAS_W_MARGIN) / canvas_m * tan(meta['pitch'])
        pitch_v[1] *= TILT_CORRECTION * meta['alt'] * (CANVAS_W - 2 * CANVAS_W_MARGIN) / canvas_m * tan(meta['pitch'])
        roll_v[0] *= TILT_CORRECTION * meta['alt'] * (CANVAS_W - 2 * CANVAS_W_MARGIN) / canvas_m * tan(meta['roll'])
        roll_v[0] *= TILT_CORRECTION * meta['alt'] * (CANVAS_W - 2 * CANVAS_W_MARGIN) / canvas_m * tan(meta['roll'])

        meta['y'] += round(pitch_v[0] + roll_v[0])
        meta['x'] += round(pitch_v[1] + roll_v[1])

        for i in range(h):
            for j in range(w):
                ci, cj = meta['y'] + i - h // 2, meta['x'] + j - w // 2
                if 0 <= ci < CANVAS_H and 0 <= cj < CANVAS_W and image[i][j][3] > 100:
                    dst = int(sqrt((h // 2 - i) ** 2 + (w // 2 - j) ** 2))
                    if nocrop or dst < dst_max:
                        min_i = min(min_i, ci)
                        max_i = max(max_i, ci)
                        min_j = min(min_j, cj)
                        max_j = max(max_j, cj)
                        if canvas[ci, cj, 3] == 0:
                            canvas[ci, cj] = image[i][j]
                            cdst[ci, cj] = dst
                        else:
                            canvas[ci, cj] = blend_by_dst(image[i][j], dst, canvas[ci, cj], cdst[ci, cj], fast_mode)
                            cdst[ci, cj] = min(dst, cdst[ci, cj])
    if not gif:
        canvas = canvas[min_i:max_i]
        canvas = canvas[:, min_j:max_j]
        return cv2.cvtColor(canvas, cv2.COLOR_RGBA2BGRA)
    frames = []
    h = int(h * 0.6)
    w = int(w * 0.6)
    for i in range(1, len(images)):
        for percent in range(1, 100, 5):
            x = int(percent / 100 * (metas[i]['x'] - metas[i - 1]['x']) + metas[i - 1]['x'])
            y = int(percent / 100 * (metas[i]['y'] - metas[i - 1]['y']) + metas[i - 1]['y'])
            frames.append(canvas[y - h // 2 : y + h // 2, x - w // 2 : x + w // 2])
    return imageio.mimwrite(imageio.RETURN_BYTES, frames, 'GIF', fps=15)

