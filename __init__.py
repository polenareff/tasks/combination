from http.server import BaseHTTPRequestHandler, HTTPServer
from math import cos, pi
import cgi, cv2, itertools, json, numpy as np

from .algo import combine, CANVAS_H, CANVAS_W, CANVAS_H_MARGIN, CANVAS_W_MARGIN, MIN_ALT, MIN_CANVAS_M, MAX_ROLL, MAX_PITCH
MAX_PHOTOS = 30

class Server(BaseHTTPRequestHandler):

    @classmethod
    def raw2img(cls, raw):
        arr = np.frombuffer(raw, dtype=np.uint8)
        img = cv2.imdecode(arr, cv2.IMREAD_COLOR)
        if img is None:
            raise Exception('Invalid image data provided!')
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGBA)
        return img

    @classmethod
    def img2raw(cls, img):
        return cv2.imencode('.png', img)[1].tostring()

    def do_POST(self):
        content_length = int(self.headers.get('Content-Length'))
        try:
            content_type, pdict = \
                    cgi.parse_header(self.headers.get('Content-Type'))
            pdict['boundary'] = pdict['boundary'].encode()
            pdict['CONTENT-LENGTH'] = content_length
            if not content_type == 'multipart/form-data':
                raise Exception('Only multipart form post allowed!')
            data = cgi.parse_multipart(self.rfile, pdict)

            data = {k: v[0] for k, v in data.items()}
            data['meta'] = json.loads(data['meta'])

            data['meta']['photos'] = list(itertools.islice((photo for photo in data['meta']['photos']
                    if photo['name'] in data
                    and photo['alt'] >= MIN_ALT
                    and ((not 'roll' in photo) or abs(photo['roll']) < MAX_ROLL)
                    and ((not 'pitch' in photo) or abs(photo['pitch']) < MAX_PITCH)), MAX_PHOTOS))

            images = {photo['name']: Server.raw2img(data[photo['name']]) for photo in data['meta']['photos']}

            # TODO: Check meta entries' format

            lat_min = min(photo['lat'] for photo in data['meta']['photos'])
            lat_max = max(photo['lat'] for photo in data['meta']['photos'])
            delta_lat = lat_max - lat_min
            lng_min = min(photo['lng'] for photo in data['meta']['photos'])
            lng_max = max(photo['lng'] for photo in data['meta']['photos'])
            delta_lng = lng_max - lng_min

            for photo in data['meta']['photos']:
                photo['lat'] = lat_min + lat_max - photo['lat']

            delta_lat_m = delta_lat * 111320
            delta_lng_m = delta_lng * 40075000 * cos((lat_max + lat_min) / 2 / 180 * pi) / 360

            canvas_m = max(delta_lat_m, delta_lng_m, MIN_CANVAS_M)

            for photo in data['meta']['photos']:
                if delta_lat < 1e-6:
                    photo['y'] = CANVAS_H / 2
                else:
                    photo['y'] = CANVAS_H_MARGIN + (CANVAS_H - 2 * CANVAS_H_MARGIN) * ((canvas_m - delta_lat_m) / 2 + delta_lat_m * (photo['lat'] - lat_min) / delta_lat) / canvas_m
                if delta_lng < 1e-6:
                    photo['x'] = CANVAS_W / 2
                else:
                    photo['x'] = CANVAS_W_MARGIN + (CANVAS_W - 2 * CANVAS_W_MARGIN) * ((canvas_m - delta_lng_m) / 2 + delta_lng_m * (photo['lng'] - lng_min) / delta_lng) / canvas_m
                photo['y'] = round(photo['y'])
                photo['x'] = round(photo['x'])

            response = combine(data['meta']['photos'], images,
                    canvas_m=canvas_m,
                    foc_len=data['meta']['drone']['foc_len'],
                    gif=('gif' in data),
                    fast_mode=(data['meta']['info']['fake'] or 'fast_mode' in data),
                    nocrop=data['meta']['info']['fake'])
            if not 'gif' in data:
                response = Server.img2raw(response)
        except:
            self.send_response(400)
            self.end_headers()
        else:
            self.send_response(200)
            self.end_headers()
            self.wfile.write(response)

httpd = HTTPServer(('0.0.0.0', 7007), Server)
httpd.serve_forever()

